# README #


### Demonstrates usage of CUDA to brute-force RAR password  ###
https://www.linkedin.com/pulse/using-cuda-brute-force-rar-password-pavel-turbin

### How do I get set up? ###

* Install CUDA SDK
* Open solution passcrack_vs2017.sln  into Visual Studio 17
* Build and run


### Who do I talk to? ###

* Contact Pavel Turbin https://www.linkedin.com/in/pavelturbin/
