// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>

#define CUDA_ON 
#include "passdef.h"
#include "byteops.h"
#include "sha1.h"
#include "rijndael.h"
#include "rijndael_static_cuda.h"
#include "impl.h"

__constant__ unsigned char DecryptState_salt[8]; // SIZE_SALT30
__constant__ unsigned char DecryptState_encrypted[CRYPTO_BUFFER_SIZES];
__constant__ unsigned char DecryptState_decrypted[CRYPTO_BUFFER_SIZES];
__constant__ unsigned int  DecryptState_bufferSize[1];



#define MAX_SHARED_CONTEXTS 256

__global__ void kernelHandlePasswords(PASSWORD_REQUEST *pPasswords, int arrayLength)
{
  int currentThreadId = blockDim.x * blockIdx.x + threadIdx.x;
  if (currentThreadId < arrayLength)
  {
    
    // possibly later it could be move to shared to speed up memory access
    //__shared__ sha1_context sharedContexts[CUDA_THREADS_PER_BLOCK+1] ;
    //sha1_context *pConext = &sharedContexts[threadIdx.x];
    //__syncthreads();
    
     sha1_context sharedContexts; 

     kernelHandlePasswordThread(&sharedContexts, &pPasswords[currentThreadId], DecryptState_salt, DecryptState_encrypted, DecryptState_bufferSize[0], DecryptState_decrypted);
  }
}



extern "C" int prepareCUDA(const int argc, const char **argv)
{
  int devID;
  cudaDeviceProp props;

  // This will pick the best possible CUDA capable device
  devID = findCudaDevice(argc, (const char **)argv);


  //Get GPU information
  checkCudaErrors(cudaGetDevice(&devID));
  checkCudaErrors(cudaGetDeviceProperties(&props, devID));

  printf("Device %d: \"%s\" with Compute %d.%d capability\n", devID, props.name, props.major, props.minor);
  printf("totalGlobalMem: %d Kb\n", (int)(props.totalGlobalMem/1024));
  printf("sharedMemPerBlock: %d, 0x%x\n", (int)props.sharedMemPerBlock, (int)props.sharedMemPerBlock);
  printf("warpSize: %d\n", props.warpSize);
  printf("maxThreadsPerBlock: %d\n", props.maxThreadsPerBlock);
  printf("maxThreadsDim[3]: %d %d %d\n", props.maxThreadsDim[0], props.maxThreadsDim[1], props.maxThreadsDim[2]);
  printf("maxGridSize[3]: %d %d %d\n", props.maxGridSize[0], props.maxGridSize[1], props.maxGridSize[2]);
  printf("clockRate %d Mhz\n", props.clockRate/1000);


  return devID;
}



extern "C" void prepareDecryptState(const DecryptState *decstate)
{
  checkCudaErrors(cudaMemcpyToSymbol(
    DecryptState_salt,
    decstate->salt,
    sizeof(decstate->salt)
  ));

  checkCudaErrors(cudaMemcpyToSymbol(
    DecryptState_encrypted,
    decstate->encrypted,
    CRYPTO_BUFFER_SIZES
  ));

  checkCudaErrors(cudaMemcpyToSymbol(
    DecryptState_decrypted,
    decstate->decrypted,
    CRYPTO_BUFFER_SIZES
  ));

  unsigned int  bufferSize[1];
  bufferSize[0] = decstate->bufferSize;
  checkCudaErrors(cudaMemcpyToSymbol(
    DecryptState_bufferSize,
    bufferSize,
    sizeof(bufferSize)
  ));

  Rijndael_GenerateTablesCuda();
}


extern "C" void processPasswordsCuda(const int devId, PASSWORD_REQUEST *pPasswords, int arrayLength)
{
  PASSWORD_REQUEST *d_Passwords;
  int bufferSize = arrayLength * sizeof(PASSWORD_REQUEST);
  checkCudaErrors(cudaMalloc(&d_Passwords, bufferSize));
  checkCudaErrors(cudaMemcpy(d_Passwords, pPasswords, bufferSize, cudaMemcpyHostToDevice));

  kernelHandlePasswords <<<CUDA_BLOCKS_PER_GRID, CUDA_THREADS_PER_BLOCK >>>(d_Passwords, arrayLength);

  checkCudaErrors(cudaGetLastError());

  checkCudaErrors(cudaDeviceSynchronize());

  cudaMemcpy(pPasswords, d_Passwords, bufferSize, cudaMemcpyDeviceToHost);
  cudaFree(d_Passwords);
}



