// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

DEVICE void fillPassword(byte *RawPsw, const byte *salt, const PASSWORD_REQUEST *pPassword)
{
#pragma unroll
  for (int i = 0; i < 2 * PASS_LEN; i += 2)
  {
    RawPsw[i] = pPassword->password[i / 2];
    RawPsw[i + 1] = 0;
  }
#pragma unroll
  for (int i = 0; i < SIZE_SALT30; i++)
  {
    RawPsw[2 * PASS_LEN + i] = salt[i];
  }
}



DEVICE int check_decryped(const byte *decrypted, int bufferSize, const byte *decryptedShouldBe)
{
  for (int i = 0; i < bufferSize; i++)
  {
    if (decrypted[i] != decryptedShouldBe[i])
      return 0;
  }
  return 1;
}



DEVICE void kernelHandlePasswordThread(sha1_context *pMainContext, PASSWORD_REQUEST *pPassword, const byte *salt, byte *encryptedBuffer, int bufferSize, byte *decryptedShouldBe)
{
  byte RawPsw[PasswordRawLength];
  byte AESKey[16], AESInit[16];
  byte     m_initVector[MAX_IV_SIZE];
  byte     m_expandedKey[_MAX_ROUNDS + 1][4][4];
  unsigned int sha1_workspace[16];
  byte PswNum[3];
  unsigned int digest[5];

  unsigned int m_uRounds = 10;


  fillPassword(RawPsw, salt, pPassword);
  sha1_context *c = pMainContext;
  sha1_init(c);

  const int HashRounds = 0x40000;
  for (int I = 0; I < HashRounds; I++)
  {
    sha1_process_rar29(c, RawPsw, sha1_workspace);
    PswNum[0] = (byte)I;
    PswNum[1] = (byte)(I >> 8);
    PswNum[2] = (byte)(I >> 16);
    sha1_process(c, PswNum, 3, sha1_workspace);
    if (I % (HashRounds / 16) == 0)
    {
      sha1_context tempc = *c;
      sha1_done(&tempc, digest);
      AESInit[I / (HashRounds / 16)] = (byte)digest[4];
    }
  }


  sha1_done(c, digest);
#pragma unroll
  for (int I = 0; I < 4; I++)
  {
    for (int J = 0; J < 4; J++)
      AESKey[I * 4 + J] = (byte)(digest[I] >> (J * 8));
  }
  Rijndael_Init(AESKey, 128, AESInit, m_uRounds, m_initVector, m_expandedKey);

  byte decrypted[CRYPTO_BUFFER_SIZES];

  Rijndael_blockDecrypt(encryptedBuffer, bufferSize, m_uRounds, decrypted, m_initVector, m_expandedKey);
  pPassword->status = check_decryped(decrypted, bufferSize, decryptedShouldBe);
}



