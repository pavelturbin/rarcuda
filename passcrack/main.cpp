// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

#include <iostream>
#include <stdlib.h>
#include <math.h>

// Required to include CUDA vector types
#include <cuda_runtime.h>
#include <vector_types.h>
#include <helper_cuda.h>
#include <windows.h>
#include "resource.h"

#include "passdef.h"


#define MAX_DICTIONARY 64
#define MAX_PASSWORD_LENGTH 20

class PasswordGenerator
{
  int  passwordLength = 0;
  int  dictionaryLength = 0;
  bool isEnd = false;

  unsigned char dictionary[MAX_DICTIONARY];
  char passwordDictionary[MAX_PASSWORD_LENGTH][MAX_DICTIONARY];
  char indexes[MAX_DICTIONARY];
  int  currentIndex = 0;

public:
  PasswordGenerator(int plength) :passwordLength(plength)
  {
    memset(indexes, 0, MAX_DICTIONARY);
    InitDictionary();
    for (int i = 0; i < MAX_PASSWORD_LENGTH; i++)
    {
      memcpy(passwordDictionary[i], dictionary, MAX_DICTIONARY);
    }
    printf("Total combinations expected: %I64d\n", (__int64)pow(dictionaryLength, passwordLength) );
  }

  bool GetNextPassword(unsigned char *dest)
  {
    if(isEnd)
      return false;

    for (int passSym = 0; passSym < passwordLength; passSym++)
    {
      dest[passSym] = passwordDictionary[passSym][indexes[passSym]];
    }
  
    if (indexes[0] + 1 >= dictionaryLength)
    {
      indexes[0] = 0;
      if (!ShiftIndexes())
      {
        isEnd = true;
        return true;
      }

    }
    else
      indexes[0] ++;
    return true;
  }

  bool GenerateRequestBatch(PASSWORD_REQUEST *pPasswords, int arrayLength, int &generated)
  {
    int current = 0;
    while (GetNextPassword(pPasswords[current].password))
    {
      if (current >= arrayLength)
        break;
      current++;
    }
    generated = current;
    return current != 0;
  }

  bool ShiftIndexes()
  {
    for (int i = 1; i < passwordLength; i++)
    {
      if (indexes[i] + 1 >= dictionaryLength)
      {
        indexes[i] = 0;
        if ((i + 1) == passwordLength)
          return false;
      }
      else
      {
        indexes[i] ++;
        return true;
      }
    }
    return true;
  }

private:

  void InitDictionary()
  {
    for (int i = 'a'; i <= 'z'; i++)
    {
      dictionary[dictionaryLength++] = i;
    }
    for (int i = '0'; i <= '9'; i++)
    {
      dictionary[dictionaryLength++] = i;
    }
  }
  void InitDictionarySmall()
  {
    for (int i = '0'; i <= '9'; i++)
    {
      dictionary[dictionaryLength++] = i;
    }
  }

};



bool testIfPasswordFound(PASSWORD_REQUEST *pPasswords, int arrayLength)
{
  for (int i = 0; i < arrayLength; i++)
  {
    if (pPasswords[i].status)
    {
      printf("found password:");
      for (int j = 0; j < PASS_LEN; j++)
      {
        printf("%c", pPasswords[i].password[j]);
      }
      printf("\n");
      return true;
    }
  }
  return false;
}

void bruteForceLoopCuda(int devId)
{
  int arrayLength = CUDA_BLOCKS_PER_GRID*CUDA_THREADS_PER_BLOCK;
  int bufferSize = arrayLength * sizeof(PASSWORD_REQUEST);
  PASSWORD_REQUEST *pPasswords = (PASSWORD_REQUEST *)malloc(bufferSize);
  memset(pPasswords, 0, bufferSize);

  printf("Using threads %d\n", arrayLength);


  int processed = 0;
  DWORD st = GetTickCount();

  int arrayLengthGenerated = 0;
  PasswordGenerator generator(MAX_PASS_LEN);
  while (generator.GenerateRequestBatch(pPasswords, arrayLength, arrayLengthGenerated))
  {
    processPasswordsCuda(devId, pPasswords, arrayLengthGenerated);
    processed += arrayLengthGenerated;

    DWORD timeRun = GetTickCount() - st;
    DWORD timeRunSec = timeRun / 1000;
    if (timeRunSec == 0)
      timeRunSec = 1;
    printf("done in %d processed=%d, %d persec\n", timeRun, processed, processed / timeRunSec);
    if (testIfPasswordFound(pPasswords, arrayLengthGenerated))
      break;

    memset(pPasswords, 0, bufferSize);
  }
  free(pPasswords);
}

int main(int argc, char **argv)
{
  DecryptState decstate;

  if (!LoadDecryptState(&decstate))
    return 0;

  int devId = prepareCUDA(argc, (const char **)argv);
  prepareDecryptState(&decstate);

  bruteForceLoopCuda(devId);


  return 0;
}

void testArray(PASSWORD_REQUEST *pPasswords, int arrayLength)
{
  for (int i = 0; i < arrayLength; i++)
  {
    if (pPasswords[i].status != 1)
    {
      printf("Bad status at %d\n", i);
      exit(0);
    }
    pPasswords[i].status = 0;
  }
}

// small self test runing fixed password to see that cracking is consistent
int main_FixedPasswordTest(int argc, char **argv) 
{
  DecryptState decstate;

  if (!LoadDecryptState(&decstate))
    return 0;


  int devId = prepareCUDA(argc, (const char **)argv);

  PASSWORD_REQUEST *pPasswords = 0;
  int arrayLength = CUDA_BLOCKS_PER_GRID*CUDA_THREADS_PER_BLOCK;
  //  int arrayLength = 1;
  int bufferSize = arrayLength * sizeof(PASSWORD_REQUEST);
  pPasswords = (PASSWORD_REQUEST *)malloc(bufferSize);
  memset(pPasswords, 0, bufferSize);

  char password[PASS_LEN] = { '1', '2', '3', '4', '5' };
  for (int i = 0; i < arrayLength; i++)
    memcpy(pPasswords[i].password, password, 5);

  prepareDecryptState(&decstate);

  DWORD st = GetTickCount();

  int processed = 0;
  for (int i = 0; i < 20; i++)
  {
    processed += arrayLength;
    processPasswordsCuda(devId, pPasswords, arrayLength);

    DWORD timeRun = GetTickCount() - st;
    DWORD timeRunSec = timeRun / 1000;
    if (timeRunSec == 0)
      timeRunSec = 1;
    printf("done in %d processed=%d, %d persec\n", timeRun, processed, processed / timeRunSec);
    testArray(pPasswords, arrayLength);
  }


  free(pPasswords);
  return 0;
}

// same code for comparing with Windows 
int main_Win32(int argc, char **argv)
{
  DecryptState decstate;

  if (!LoadDecryptState(&decstate))
    return 0;

  PASSWORD_REQUEST *pPasswords = 0;
  int arrayLength = 1000;
  int bufferSize = arrayLength * sizeof(PASSWORD_REQUEST);
  pPasswords = (PASSWORD_REQUEST *)malloc(bufferSize);
  memset(pPasswords, 0, bufferSize);

  char password[PASS_LEN] = { '1', '2', '3', '4', '5' };
  for (int i = 0; i < arrayLength; i++)
    memcpy(pPasswords[i].password, password, 5);


  Rijndael_GenerateTablesWin32();

  DWORD st = GetTickCount();

  int processed = 0;
  for (int i = 0; i < 2000; i++)
  {
    processed += arrayLength;
    processPasswordsWin32(&decstate, pPasswords, arrayLength);

    DWORD timeRun = GetTickCount() - st;
    DWORD timeRunSec = timeRun / 1000;
    if (timeRunSec == 0)
      timeRunSec = 1;
    printf("done in %d processed=%d, %d persec\n", timeRun, processed, processed / timeRunSec);

    testArray(pPasswords, arrayLength);
  }


  free(pPasswords);
  return 0;
}



