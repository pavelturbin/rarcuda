// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

#define  rotls(x,n,xsize)  (((x)<<(n)) | ((x)>>(xsize-(n))))
#define  rotrs(x,n,xsize)  (((x)>>(n)) | ((x)<<(xsize-(n))))
#define  rotl32(x,n)       rotls(x,n,32)
#define  rotr32(x,n)       rotrs(x,n,32)


DEVICE inline unsigned int  ByteSwap32(unsigned int  i)
{
  return (rotl32(i, 24) & 0xFF00FF00) | (rotl32(i, 8) & 0x00FF00FF);
}

DEVICE inline void RawPut4(unsigned int  Field, void *Data)
{
  *(unsigned int *)Data = Field;
}

DEVICE  inline void RawPutBE4(unsigned int i, byte *mem)
{
  mem[0] = byte(i >> 24);
  mem[1] = byte(i >> 16);
  mem[2] = byte(i >> 8);
  mem[3] = byte(i);
}


DEVICE inline void Xor128(void *dest, const void *arg1, const void *arg2)
{
#pragma unroll
  for (int I = 0; I<16; I++)
    ((byte*)dest)[I] = ((byte*)arg1)[I] ^ ((byte*)arg2)[I];
}

DEVICE inline void Xor128(byte *dest, const byte *arg1, const byte *arg2, const byte *arg3, const byte *arg4)
{
#pragma unroll
  for (int I = 0; I<4; I++)
    dest[I] = arg1[I] ^ arg2[I] ^ arg3[I] ^ arg4[I];
}


DEVICE inline void Copy128(byte *dest, const byte *src)
{
#pragma unroll
  for (int I = 0; I<16; I++)
    dest[I] = src[I];
}

DEVICE inline void  memcpyCustom(byte *dest, const byte *src, int n) 
{
  byte *dp = (byte *)dest; 
  const byte *sp = (const byte *)src; 
  while (n--) 
    *dp++ = *sp++; 
}


