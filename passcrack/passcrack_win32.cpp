#include <windows.h>
#include <stdio.h>

#include "passdef.h"
#include "byteops.h"
#include "sha1.h"
#include "rijndael.h"
#include "rijndael_static_win32.h"
#include "impl.h"

#define MAX_THREADS 16

struct ThreadContext
{
  int           startIndex;
  int           endIndex;
  DecryptState *state;
  PASSWORD_REQUEST *pPasswords;
};

DWORD __stdcall ProcessingThreadFunction(PVOID ctx)
{
  ThreadContext *context = (ThreadContext*)ctx;
  for (int i = context->startIndex; i < context->endIndex; i++)
  {
    sha1_context MainContext;
    kernelHandlePasswordThread(&MainContext, &context->pPasswords[i], context->state->salt, context->state->encrypted, context->state->bufferSize, context->state->decrypted);
  }

  delete context;
  return 0;
}

void startProcessingThread(DecryptState *state, PASSWORD_REQUEST *pPasswords, int arrayLength, int &threadIndex, HANDLE *waits)
{

  int batchsize = arrayLength < MAX_THREADS ? arrayLength : (arrayLength / MAX_THREADS)+1;
  int startIndex = threadIndex*batchsize;
  if (startIndex >= arrayLength)
    return;

  ThreadContext *context = new ThreadContext;
  context->startIndex = startIndex;
  context->endIndex = context->startIndex + batchsize;
  if (context->endIndex >= arrayLength)
    context->endIndex = arrayLength;

  context->state = state;
  context->pPasswords = pPasswords;


  DWORD ThreadId;
  waits[threadIndex] = CreateThread(0, 0, ProcessingThreadFunction, context, 0, &ThreadId);
  threadIndex++;
}


extern "C" void processPasswordsWin32( DecryptState *state, PASSWORD_REQUEST *pPasswords, int arrayLength)
{
  int threadIndex = 0;
  HANDLE waits[MAX_THREADS+1];
  for (int i = 0; i < MAX_THREADS; i++)
  {
    startProcessingThread(state, pPasswords, arrayLength, threadIndex, waits);
  }

  DWORD stat = WaitForMultipleObjects(threadIndex, waits, TRUE, INFINITE);
  if (stat != 0)
  {
    printf("thread waiting failed %d\n", stat);
    exit(1);
  }

  for (int i = 0; i < threadIndex; i++)
  {
    CloseHandle(waits[i]);
  }



}

