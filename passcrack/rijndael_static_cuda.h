extern "C" void Rijndael_GenerateTablesCuda()
{
  byte S_local[256], S5_local[256], rcon_local[30];
  byte T1_local[256][4], T2_local[256][4], T3_local[256][4], T4_local[256][4];
  byte T5_local[256][4], T6_local[256][4], T7_local[256][4], T8_local[256][4];
  byte U1_local[256][4], U2_local[256][4], U3_local[256][4], U4_local[256][4];


  unsigned char pow[512], log[256];
  int i = 0, w = 1;
  do
  {
    pow[i] = (byte)w;
    pow[i + 255] = (byte)w;
    log[w] = (byte)i++;
    w ^= (w << 1) ^ (w & ff_hi ? ff_poly : 0);
  } while (w != 1);

  for (int i = 0, w = 1; i < sizeof(rcon_local) / sizeof(rcon_local[0]); i++)
  {
    rcon_local[i] = w;
    w = (w << 1) ^ (w & ff_hi ? ff_poly : 0);
  }
  for (int i = 0; i < 256; ++i)
  {
    unsigned char b = S_local[i] = fwd_affine(FFinv((byte)i));
    T1_local[i][1] = T1_local[i][2] = T2_local[i][2] = T2_local[i][3] = T3_local[i][0] = T3_local[i][3] = T4_local[i][0] = T4_local[i][1] = b;
    T1_local[i][0] = T2_local[i][1] = T3_local[i][2] = T4_local[i][3] = FFmul02(b);
    T1_local[i][3] = T2_local[i][0] = T3_local[i][1] = T4_local[i][2] = FFmul03(b);
    S5_local[i] = b = FFinv(inv_affine((byte)i));
    U1_local[b][3] = U2_local[b][0] = U3_local[b][1] = U4_local[b][2] = T5_local[i][3] = T6_local[i][0] = T7_local[i][1] = T8_local[i][2] = FFmul0b(b);
    U1_local[b][1] = U2_local[b][2] = U3_local[b][3] = U4_local[b][0] = T5_local[i][1] = T6_local[i][2] = T7_local[i][3] = T8_local[i][0] = FFmul09(b);
    U1_local[b][2] = U2_local[b][3] = U3_local[b][0] = U4_local[b][1] = T5_local[i][2] = T6_local[i][3] = T7_local[i][0] = T8_local[i][1] = FFmul0d(b);
    U1_local[b][0] = U2_local[b][1] = U3_local[b][2] = U4_local[b][3] = T5_local[i][0] = T6_local[i][1] = T7_local[i][2] = T8_local[i][3] = FFmul0e(b);
  }


  Rijndael_injectVar(
    S,
    S_local,
    sizeof(S_local)
  );

  Rijndael_injectVar(
    S5,
    S5_local,
    sizeof(S5_local)
  );

  Rijndael_injectVar(
    rcon,
    rcon_local,
    sizeof(rcon_local)
  );
  Rijndael_injectVar(
    T1,
    T1_local,
    sizeof(T1_local)
  );
  Rijndael_injectVar(
    T2,
    T2_local,
    sizeof(T2_local)
  );
  Rijndael_injectVar(
    T3,
    T3_local,
    sizeof(T3_local)
  );
  Rijndael_injectVar(
    T4,
    T4_local,
    sizeof(T4_local)
  );
  Rijndael_injectVar(
    T5,
    T5_local,
    sizeof(T5_local)
  );
  Rijndael_injectVar(
    T6,
    T6_local,
    sizeof(T6_local)
  );
  Rijndael_injectVar(
    T7,
    T7_local,
    sizeof(T7_local)
  );
  Rijndael_injectVar(
    T8,
    T8_local,
    sizeof(T8_local)
  );
  Rijndael_injectVar(
    U1,
    U1_local,
    sizeof(U1_local)
  );
  Rijndael_injectVar(
    U2,
    U2_local,
    sizeof(U2_local)
  );
  Rijndael_injectVar(
    U3,
    U3_local,
    sizeof(U3_local)
  );
  Rijndael_injectVar(
    U4,
    U4_local,
    sizeof(U4_local)
  );
}


