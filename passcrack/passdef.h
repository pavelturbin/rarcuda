// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

#pragma once
#include "..\common\decyptstate.h"

#define MAX_PASS_LEN 8 
#define PASS_LEN 5 
#define SIZE_SALT30 8

#define _MAX_KEY_COLUMNS (256/32)
#define _MAX_ROUNDS      14
#define MAX_IV_SIZE      16
#define CRYPTO_BUFFER_SIZES 0x64

const int PasswordRawLength = 2 * PASS_LEN + SIZE_SALT30;


#define CUDA_BLOCKS_PER_GRID 16
#define CUDA_THREADS_PER_BLOCK 256



struct PASSWORD_REQUEST
{
  unsigned char     password[MAX_PASS_LEN];
  int               length;
  int               status;
};

#ifdef CUDA_ON
#define DEVICE __device__
#define CONSTANT __constant__
#define SHARED __shared__
#define SYNCTHREADS __syncthreads()

#else
#define DEVICE static
#define CONSTANT static
#define SHARED 
#define SYNCTHREADS while(0)
#endif

extern "C" int prepareCUDA(const int argc, const char **argv);
extern "C" void prepareDecryptState(const DecryptState *decstate);
extern "C" void Rijndael_GenerateTablesWin32();

extern "C" void processPasswordsCuda(const int devId,
                                 PASSWORD_REQUEST *pPasswords, int arrayLength);

extern "C" void processPasswordsWin32(DecryptState *state, PASSWORD_REQUEST *pPasswords, int arrayLength);

bool LoadDecryptState(DecryptState *decstate);


