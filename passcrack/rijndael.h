CONSTANT byte S[256], S5[256], rcon[30];
CONSTANT  byte T1[256][4], T2[256][4], T3[256][4], T4[256][4];
CONSTANT  byte T5[256][4], T6[256][4], T7[256][4], T8[256][4];
CONSTANT  byte U1[256][4], U2[256][4], U3[256][4], U4[256][4];


DEVICE void Rijndael_keySched(byte key[_MAX_KEY_COLUMNS][4], int m_uRounds, byte m_expandedKey[_MAX_ROUNDS + 1][4][4])
{
  int j, rconpointer = 0;

  // Calculate the necessary round keys
  // The number of calculations depends on keyBits and blockBits
  int uKeyColumns = m_uRounds - 6;

  byte tempKey[_MAX_KEY_COLUMNS][4];

  // Copy the input key to the temporary key matrix

  memcpy(tempKey, key, sizeof(tempKey));

  int r = 0;
  int t = 0;

  // copy values into round key array
  for (j = 0; (j < uKeyColumns) && (r <= m_uRounds); )
  {
    for (; (j < uKeyColumns) && (t < 4); j++, t++)
      for (int k = 0; k<4; k++)
        m_expandedKey[r][t][k] = tempKey[j][k];

    if (t == 4)
    {
      r++;
      t = 0;
    }
  }

  while (r <= m_uRounds)
  {
    tempKey[0][0] ^= S[tempKey[uKeyColumns - 1][1]];
    tempKey[0][1] ^= S[tempKey[uKeyColumns - 1][2]];
    tempKey[0][2] ^= S[tempKey[uKeyColumns - 1][3]];
    tempKey[0][3] ^= S[tempKey[uKeyColumns - 1][0]];
    tempKey[0][0] ^= rcon[rconpointer++];

    if (uKeyColumns != 8)
      for (j = 1; j < uKeyColumns; j++)
        for (int k = 0; k<4; k++)
          tempKey[j][k] ^= tempKey[j - 1][k];
    else
    {
      for (j = 1; j < uKeyColumns / 2; j++)
        for (int k = 0; k<4; k++)
          tempKey[j][k] ^= tempKey[j - 1][k];

      tempKey[uKeyColumns / 2][0] ^= S[tempKey[uKeyColumns / 2 - 1][0]];
      tempKey[uKeyColumns / 2][1] ^= S[tempKey[uKeyColumns / 2 - 1][1]];
      tempKey[uKeyColumns / 2][2] ^= S[tempKey[uKeyColumns / 2 - 1][2]];
      tempKey[uKeyColumns / 2][3] ^= S[tempKey[uKeyColumns / 2 - 1][3]];
      for (j = uKeyColumns / 2 + 1; j < uKeyColumns; j++)
        for (int k = 0; k<4; k++)
          tempKey[j][k] ^= tempKey[j - 1][k];
    }
    for (j = 0; (j < uKeyColumns) && (r <= m_uRounds); )
    {
      for (; (j < uKeyColumns) && (t < 4); j++, t++)
        for (int k = 0; k<4; k++)
          m_expandedKey[r][t][k] = tempKey[j][k];
      if (t == 4)
      {
        r++;
        t = 0;
      }
    }
  }
}

DEVICE void Rijndael_keyEncToDec(int m_uRounds, byte m_expandedKey[_MAX_ROUNDS + 1][4][4])
{
  for (int r = 1; r < m_uRounds; r++)
  {
    byte n_expandedKey[4][4];
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
      {
        byte *w = m_expandedKey[r][j];
        n_expandedKey[j][i] = U1[w[0]][i] ^ U2[w[1]][i] ^ U3[w[2]][i] ^ U4[w[3]][i];
      } 
    memcpy(m_expandedKey[r], n_expandedKey, sizeof(m_expandedKey[0]));
  }
}


DEVICE void Rijndael_Init(const byte *key, unsigned int keyLen, const byte * initVector, int m_uRounds, byte *m_initVector, byte m_expandedKey[_MAX_ROUNDS + 1][4][4])
{
  const unsigned int uKeyLenInBytes = 16;
  byte keyMatrix[_MAX_KEY_COLUMNS][4];

  for (unsigned int i = 0; i < uKeyLenInBytes; i++)
    keyMatrix[i >> 2][i & 3] = key[i];
  for (int i = 0; i < MAX_IV_SIZE; i++)
    m_initVector[i] = initVector[i];
  Rijndael_keySched(keyMatrix, m_uRounds, m_expandedKey);

  Rijndael_keyEncToDec(m_uRounds, m_expandedKey);
}

DEVICE void Rijndael_blockDecrypt(const byte *input, size_t inputLen, unsigned int m_uRounds, byte *outBuffer, byte *m_initVector, byte m_expandedKey[_MAX_ROUNDS + 1][4][4])
{
  if (inputLen <= 0)
    return;

  size_t numBlocks = inputLen / 16;

  byte block[16], iv[4][4];
  memcpy(iv, m_initVector, 16);

  for (size_t i = numBlocks; i > 0; i--)
  {
    byte temp[4][4];

    Xor128(temp, input, m_expandedKey[m_uRounds]);

    Xor128(block, T5[temp[0][0]], T6[temp[3][1]], T7[temp[2][2]], T8[temp[1][3]]);
    Xor128(block + 4, T5[temp[1][0]], T6[temp[0][1]], T7[temp[3][2]], T8[temp[2][3]]);
    Xor128(block + 8, T5[temp[2][0]], T6[temp[1][1]], T7[temp[0][2]], T8[temp[3][3]]);
    Xor128(block + 12, T5[temp[3][0]], T6[temp[2][1]], T7[temp[1][2]], T8[temp[0][3]]);

    for (int r = m_uRounds - 1; r > 1; r--)
    {
      Xor128(temp, block, m_expandedKey[r]);
      Xor128(block, T5[temp[0][0]], T6[temp[3][1]], T7[temp[2][2]], T8[temp[1][3]]);
      Xor128(block + 4, T5[temp[1][0]], T6[temp[0][1]], T7[temp[3][2]], T8[temp[2][3]]);
      Xor128(block + 8, T5[temp[2][0]], T6[temp[1][1]], T7[temp[0][2]], T8[temp[3][3]]);
      Xor128(block + 12, T5[temp[3][0]], T6[temp[2][1]], T7[temp[1][2]], T8[temp[0][3]]);
    }

    Xor128(temp, block, m_expandedKey[1]);
    block[0] = S5[temp[0][0]];
    block[1] = S5[temp[3][1]];
    block[2] = S5[temp[2][2]];
    block[3] = S5[temp[1][3]];
    block[4] = S5[temp[1][0]];
    block[5] = S5[temp[0][1]];
    block[6] = S5[temp[3][2]];
    block[7] = S5[temp[2][3]];
    block[8] = S5[temp[2][0]];
    block[9] = S5[temp[1][1]];
    block[10] = S5[temp[0][2]];
    block[11] = S5[temp[3][3]];
    block[12] = S5[temp[3][0]];
    block[13] = S5[temp[2][1]];
    block[14] = S5[temp[1][2]];
    block[15] = S5[temp[0][3]];
    Xor128(block, block, m_expandedKey[0]);

    Xor128(block, block, iv);

    Copy128((byte*)iv, input);
    Copy128(outBuffer, block);

    input += 16;
    outBuffer += 16;
  }

  memcpy(m_initVector, iv, 16);
}

#define ff_poly 0x011b
#define ff_hi   0x80

#define FFinv(x)    ((x) ? pow[255 - log[x]]: 0)

#define FFmul02(x) (x ? pow[log[x] + 0x19] : 0)
#define FFmul03(x) (x ? pow[log[x] + 0x01] : 0)
#define FFmul09(x) (x ? pow[log[x] + 0xc7] : 0)
#define FFmul0b(x) (x ? pow[log[x] + 0x68] : 0)
#define FFmul0d(x) (x ? pow[log[x] + 0xee] : 0)
#define FFmul0e(x) (x ? pow[log[x] + 0xdf] : 0)
#define fwd_affine(x) \
    (w = (unsigned int)x, w ^= (w<<1)^(w<<2)^(w<<3)^(w<<4), (byte)(0x63^(w^(w>>8))))

#define inv_affine(x) \
    (w = (unsigned int)x, w = (w<<1)^(w<<3)^(w<<6), (byte)(0x05^(w^(w>>8))))


static void Rijndael_injectVar(const void *symbol, const void *src, size_t count)
{
#ifdef CUDA_ON
  checkCudaErrors(cudaMemcpyToSymbol(
    symbol,
    src,
    count
  ));
#else
  memcpy((void*)symbol, src, count);
#endif
}




