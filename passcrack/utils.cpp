// Cracking RAR passwords with CUDA
// Copyright (C) Pavel Turbin, 2017.

#include <windows.h>

#include "passdef.h"
#include "resource.h"
bool LoadDecryptState(DecryptState *decstate)
{
  HGLOBAL     res_handle = NULL;
  HRSRC       res;
  char *      res_data;
  DWORD       res_size;

  res = FindResource(NULL, MAKEINTRESOURCE(TEST_DECRYPSTATE_ID), RT_RCDATA);
  if (!res)
    return false;
  res_handle = LoadResource(NULL, res);
  if (!res_handle)
    return false;
  res_data = (char*)LockResource(res_handle);
  res_size = SizeofResource(NULL, res);
  memcpy(decstate, res_data, sizeof(DecryptState));

  return true;
}



