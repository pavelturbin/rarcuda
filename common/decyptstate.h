#pragma once

struct DecryptState
{
  unsigned char salt[8]; // SIZE_SALT30
  int bufferSize;
  unsigned char encrypted[0x64];
  unsigned char decrypted[0x64];
};


